Seating Tool


A quick & dirty tool I created to allow a coworker to quickly create classroom
seating charts for a presentation.  


Controls:
	-Up Arrow = Create new seat
	-Down Arrow = Delete selected seat
	-Left & Right Arrow = Rotate selected seat
	-"W" Key = Save .png of current scene

	-Click and drag seats to move them.

Issues:
	-Seats do not center on the cursor.
	-There is no option to "clear all" - all seats must be deleted
	 manually.


This project includes a Bitmap image package created by "Andreas" at
Flashutvecklaren.se, which can be found at:
	http://www.flashutvecklaren.se/info/en/save-image-locally.php


Copyright Jesse Anderson, 2014.