package  
{
	import BaseClasses.Actors.Actor;
	import BaseClasses.Game;
	import BaseClasses.Utilities.Keycodes;
	import Classes.Actors.Seat;
	import Classes.External.se.flashutvecklaren.bitmap.BitmapSaver;
	import flash.display.Bitmap;
	import flash.display.BitmapData;
	import flash.display.Stage;
	import flash.events.Event;
	import flash.events.MouseEvent;
	import flash.utils.getQualifiedClassName;
	import flash.utils.getDefinitionByName;
	import flash.events.KeyboardEvent;
	
	/**
	 * ...
	 * @author VisA
	 */
	public class startupGame extends Game 
	{
		private var displayList:Array = new Array();
		
		public function startupGame(stage:Stage):void
		{
			stage.addEventListener(Event.ENTER_FRAME, frameLoop);
			stage.addEventListener(MouseEvent.MOUSE_DOWN, matchWithSymbol);
			stage.addEventListener(MouseEvent.MOUSE_UP, unmatchWithSymbol);
			stage.addEventListener(KeyboardEvent.KEY_UP, keyPressHandler);
		}
		
		private function frameLoop(e:Event):void
		{
			update();
		}
		
		override public function update():void
		{
			if (displayList.length > 0)
			{
				for (var i:int = 0; i <= displayList.length-1; i++)
				{
					displayList[i].update();
					
					if (displayList[i].isFlagged)
					{
						var target:Seat = displayList[i];
						
						target.centerX = mouseX;
						target.centerY = mouseY;
					}
				}
			}
		}
		
		private function keyPressHandler(e:KeyboardEvent):void
		{
			switch(e.keyCode)
			{
				case Keycodes.W_KEY:
					takeSnapshot();
					break;
				case Keycodes.UP_KEY:
					createSeat();
					break;
				case Keycodes.DOWN_KEY:
					deleteSeat();
					break;
				case Keycodes.LEFT_KEY:
					rotateShape("negative", 15);
					break;
				case Keycodes.RIGHT_KEY:
					rotateShape("positive", 15);
					break;
				default:
					//default
			}
		}
		
		private function rotateShape(direction:String, degrees:int):void
		{
			if (displayList.length > 0)
			{
				for (var i:int = 0; i <= displayList.length-1; i++)
				{
					if (displayList[i].isFlagged)
					{
						if (direction == "negative")
						{
							trace("neg");
							displayList[i].nextRotation += degrees;
							displayList[i].isNeg = true;
						}
						
						else if (direction == "positive")
						{
							trace("pos");
							displayList[i].nextRotation += degrees;
							displayList[i].isNeg = false;
						}
					}
				}
			}
		}
		
		private function matchWithSymbol(e:Event):void
		{
			if (getClass(e.target) == Seat)
			{
				e.target.isFlagged = true;
			}
		}
		
		private function unmatchWithSymbol(e:Event):void
		{
			for (var i:int = 0; i <= displayList.length-1; i++)
			{
				displayList[i].isFlagged = false;
			}
		}
		
		private function getClass(obj:Object):Class 
		{
			return Class(getDefinitionByName(getQualifiedClassName(obj)));
		}
		
		private function createSeat():void
		{
			var gameActor:Seat = new Seat();
			displayList.push(gameActor);
			addChild(gameActor);
		}
		
		private function deleteSeat():void
		{
			
			if (displayList.length > 0)
			{
				for (var i:int = 0; i <= displayList.length-1; i++)
				{
					if (displayList[i].isFlagged)
					{
						removeChild(displayList[i]);
						displayList.splice(i, 1);
					}
				}
			}
			
			//var target:Seat = displayList[getFlaggedObject()];
			//removeChild(target);
			//displayList.splice(getFlaggedObject, 1);
			
		}
		
		private function getFlaggedObject():int
		{
			var target:int;
			
			if (displayList.length > 0)
			{
				for (var i:int = 0; i <= displayList.length-1; i++)
				{
					if (displayList[i].isFlagged)
					{
						target = i;
					}
				}
			}
			
			return target;
		}
		
		private function takeSnapshot():void
		{
			var bd:BitmapData = new BitmapData(stage.stageWidth, stage.stageHeight);
			bd.draw(stage);
			var bitmap:Bitmap = new Bitmap(bd);
			
			var tBS:BitmapSaver = new BitmapSaver();
			tBS.savePNG(bitmap);
		}
		
	}

}