package Classes.Actors 
{
	import BaseClasses.Actors.Actor;
	import flash.display.Bitmap;
	import flash.display.DisplayObject;
	import flash.display.Sprite;
	import flash.geom.Matrix;
	import flash.geom.Point;
	import flash.globalization.LocaleID;
	
	/**
	 * ...
	 * @author VisA
	 */
	public class Seat extends Actor 
	{
		[Embed(source = "../../Assets/Seat_Graphic.png")]
		private var seatGraphicClass:Class;
		private var seatGraphic:Bitmap = new seatGraphicClass();
		
		public var isFlagged:Boolean = false;
		
		public var nextRotation:int = 0;
		public var isNeg:Boolean = false;
		private var rotationPoint:Point = new Point(centerX, centerY);
		
		
		public function Seat() 
		{
			draw();
		}
		
		override public function update():void
		{
			if (nextRotation != 0)
			{
				if (isNeg)
				{
					trace("isNeg");
					rotateAroundCenter(seatGraphic, -nextRotation, rotationPoint);
				}
				
				else
				{
					trace("isPos");
					rotateAroundCenter(seatGraphic, nextRotation, rotationPoint);
				}
				
				seatGraphic.x = seatGraphic.width / 2;
				seatGraphic.y = seatGraphic.height/ 2;
				nextRotation = 0;
			}
		}
		
		override public function draw():void
		{
			//this.graphics.beginFill(0x000000);
			//this.graphics.drawRect(0, 0, 15, 15);
			//this.graphics.endFill();
			seatGraphic.width = seatGraphic.width / 2;
			seatGraphic.height = seatGraphic.height / 2;
			
			seatGraphic.x = this.centerX;
			seatGraphic.y = this.centerY;
			addChild(seatGraphic) as Sprite;
		}
		
		private function rotateAroundCenter(object:DisplayObject, angleDegrees:Number, ptRotationPoint:Point):void
		{
			var m:Matrix=object.transform.matrix;
			m.tx -= ptRotationPoint.x;
			m.ty -= ptRotationPoint.y;
			m.rotate (angleDegrees*(Math.PI/180));
			m.tx += ptRotationPoint.x;
			m.ty += ptRotationPoint.y;
			object.transform.matrix=m;
		}
	}

}