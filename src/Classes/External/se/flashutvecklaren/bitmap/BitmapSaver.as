/**
 * @author: Andreas
 * @copyright: Flashutvecklaren.se All rights reserved.
 */
package Classes.External.se.flashutvecklaren.bitmap 
{
   import flash.display.BitmapData;
   import flash.display.DisplayObject;
   import flash.net.FileReference;
   import flash.utils.ByteArray;
   
   import mx.graphics.codec.IImageEncoder;
   import mx.graphics.codec.JPEGEncoder;
   import mx.graphics.codec.PNGEncoder;
   /**
    *  Class for saving an image of a DisplayObject to the users file system.
    *  The image can be saved using PNG or JPG compression.
    */
   public class BitmapSaver
   {
      /**
       *  Constructor.
       */
      public function BitmapSaver()
      {
      }
      /**
       *  Saves an images of a DisplayObject as a PNG file on the users filesystem.
       *  @param rSource the DisplayObject to save an image of
       *  @see #saveJPG() 
       */
      public function savePNG(rSource:DisplayObject):void
      {
		  //Original
         //save(rSource, new PNGEncoder(), '.png');
		 
		 //My Modification
		 save(rSource, new PNGEncoder(), 'snapshot.png');
      }
      /**
       *  Saves an images of a DisplayObject as a JPG file on the users filesystem.
       *  @param rSource the DisplayObject to save an image of
       *  @param rQuality a value between 0.0 and 100.0. The smaller the quality value, the smaller the file size of the resultant image. The default value is 50.0.
       *  @see #savePNG()
       */
      public function saveJPG(rSource:DisplayObject, rQuality:Number = 50.0):void
      {
         save(rSource, new JPEGEncoder(rQuality), '.jpg');
      }
      /**
       *  @private
       *  Used for capturing and saving the image
       */
      protected function save(rSource:DisplayObject, rEncoder:IImageEncoder, rDefaultFileName:String):void
      {
         var tBD:BitmapData = new BitmapData(rSource.width, rSource.height);
         tBD.draw(rSource);
         var tBA:ByteArray = rEncoder.encode(tBD);
         var tFR:FileReference = new FileReference();
         tFR.save(tBA, rDefaultFileName);
         tBD.dispose();
         tBA.clear();
      }
   }
}









/*
 * 
 * 
All you need to do to save an image is to create an instance of the BitmapSaver and then call the save 
method of your choice, as shown bellow, with the DisplayObject as a parameter.

var tBS:BitmapSaver = new BitmapSaver();
tBS.savePNG(displayObjectToBeSaved);

One thing to remember is that Flash only allows you to saving files to the users desktop as part of a user 
event like clicking on a button or pressing a key. It is not possible to save an image without the user
initiating the save. The save methods will open a dialog box that lets the user save the image to the local 
filesystem. The dialog box will ask the user to enter a filename and select a location on the local computer 
to save the file. The text field for the file name will be pre populated with the right file extension.

*/


