package BaseClasses.Actors 
{
	import flash.display.Shape;
	import flash.display.Sprite;
	/**
	 * ...
	 * @author VisA
	 */
	public class Actor extends Sprite
	{
		private var _centeredX:int;
		private var _centeredY:int;
		
		public function Actor() 
		{
			
		}
		
		public function update():void
		{
			
		}

		public function draw():void
		{
			
		}
		
		public function destroy():void
		{
			
		}
		
		public function get centerX():int
		{
			var returnVal:int = x + (this.width / 2);
			return returnVal;
		}
		
		public function set centerX(i:int):void
		{
			var setVal:int = i - (this.width / 2);
			this.x = setVal;
		}
		
		public function get centerY():int
		{
			var returnVal:int = y + (this.height / 2);
			return returnVal;
		}
		
		public function set centerY(i:int):void
		{
			var setVal:int = i - (this.height / 2);
			this.y = setVal;
		}
	}

}